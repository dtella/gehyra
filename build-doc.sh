#!/bin/bash
# Generate documents from source code

SRC="doc"                      # source dir
DOC="site"                     # target dir

sphinx-build "$@" "$SRC" "$DOC"
