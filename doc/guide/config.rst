.. _guide-config:

Configuration
=============


Gehyra networks
---------------

A *Gehyra network* represents a mutually-trusting group of users. In a traditional DC network, all users connect to a central hub, which is what holds the system together. By contrast, in a Gehyra network, users connect to each other, guided by the *identity file*. Every network has one of these - it defines various things such as allowed IP ranges, encryption keys, and so on - see :ref:`guide-config-identity` for details.

The individual instances of Gehyra which run on users' computers are called *nodes*. Network operators can choose to run *bridge nodes*, which have more authority than normal nodes - we'll go into these later. TODO

For each network, Gehyra keeps track of the *configuration file*, *identity file*, *state file*, and the logs for that network.


Finding your files
------------------

User configuration data is held in ``~/.gehyra``, including Windows. Each node wishing to run will need access to each of the following.  By default, 

``NETWORK.cfg``
    The configuration file.  This is the main configuration file and is the one pointed to on the command line when running gehyra.  This file contains paths for the ``.id``, ``.db`` and ``.log`` files, defaulting to files with the same stem name.  It also contains settings related to the ports which the gehra node uses.  If the node is running as a bridge node, addition bridge specific details will be included.

``NETWORK.id``
    The identity file. This contains information such as permitted subnets, bootstrap sources and the network symetric key.  This should not be altered by anyone other than the network operator, as altering it will usually result in being unable to connect to the network.

``NETWORK.db``
    The state file.  This contains dynamic runtime information from the network, such as more reciently seen nodes, and will be automatically updated by the node when running.  This should not be altered by hand.

``NETWORK.log[.n]``
    Logs for the network. By default, this only includes program notices, and not (e.g.) chat messages.


.. _guide-config-identity:

Network identity
----------------

These settings are initially defined by the network operator. They may be edited by users, but changing some settings (such as the encryption keys) will cause their node to be rejected by the rest of the network.

``network``
    This section contains static "hub" configuration, such as network encryption key and the IP addresses allowed to connect to it. This defines the unique identity of your network.

    ``hub_name``
        The hub name, as seen by DC clients. By convention, this is often of the form "Dtella@____".
    ``hub_desc``
        The description of the "hub". If this is omitted, it will default to "[version string] on hub_name".
    ``sym_key``
        The symmetric key used to encrypt node-node communications.
    ``pub_key``
        The public key for the master network signing key.  This allows all nodes to verify bridge communications without needing the bootstrap information.
    ``subnets``
        List of CIDR subnets; Gehyra will only talk to nodes from these subnets. (Bridge nodes are exempt from this; but they must provide authentication.)
    ``valid_protocol``
        The protocol used to communicate with the client. Valid values are ``NMDC`` or ``ADC``. This may be reflected in the address that the client uses to connect to Gehyra - either a ``nmdc://`` or ``adc://`` address.
    ``force_crypto``
        Forces all client-client transfers to be in encrypted mode. Only effective if ``client_proto`` supports this - ADC does; NMDC does not.

``bootstrap``
    This section defines bootstrap data sources and the method to pull them. Each data source has a ``NAME``, which can be chosen at the operator's discretion.

    ``NAME.type``
        Type of data source. TODO
    ``NAME.args``
        Arguments passed to the puller for this data source.
    ``priority``
        A list of all the data source ``NAME``\ s. This defines an order of priority for running the bootstrap pullers.
    ``minshare_cap``
        This enforces a maximum cap for the ``minshare`` value which appears in bootstrap data. It should be set to some sane value to prevent operators from setting ``minshare = 99999TiB``, which effectively disables the network.

``location``
    TODO, just c+p description from existing config file.


User settings
-------------

Users are free to set these at their discretion. If excluded or left with empty types, the internal defaults will be substituted.

``node``
    Node specific settings,  including:

    ``interface``
        The network interface for Gehyra to bind to, as an IP string. Defaults to all interfaces.
    ``udp``
        The UDP port to use for node-node communication. Defaults to a random port.
    ``tcp``
        The TCP port for use in node to node communication. Defaults to a random port.
    ``entities``
        List of 2-tuples containing a string of the entity name and a dictionary of parameters.  The default for most nodes should contain an ADC client and dtella bot entity. TODO explain this better...


Bridge configuration
--------------------

These options are only available to bridge nodes. TODO copy rest of descriptions from bridge config file.

``network``
    Network settings

    ``key``
        @TYPE TBD@ containing the bridge private key and the bridge public key signed by the master network key.

``irc``
    This section defines IRC servers, and other IRC-related settings. Each server has a ``NAME``, which can be chosen at the operator's discretion.

    ``NAME.module``
        The IRC module name to use.
    ``NAME.etc``
        TODO copy from bridge config file
    ``default``
        A list of ``NAME``\ s of servers to connect to, if Gehyra is not otherwise explicitly instructed (eg. on the command line).

``boot``
    This section defines bootstrap data sources, how to push to them, and other related settings. Each data source has a ``NAME``, which can be chosen at the operator's discretion.

    ``NAME.type``
        Type of data source. TODO
    ``NAME.args``
        Arguments passed to the pusher for this data source.
    ``default``
        A list of ``NAME``\ s of data sources to push to by default, if Gehyra is not otherwise explicitly instructed (eg. on the command line).

