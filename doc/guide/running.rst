.. _guide-running:

Running Gehyra
==============

Command Line arguments
----------------------

Usage: gehyra [options] [config.cfg]

Run Gehyra with the given configuration file, or the default if none is given.

Options:
    --version                           Show program's version number and exit.
    -h, --help                          Show this help message and exit.
    -c, --config=PATH                   Specifiy the path to the configuration file.  This option is implicit if ignored.
    -m, --make-config                   Write the default configuration into the specified config file and exit.
    -t, --terminate                     Attempt to terminate an already-running Gehyra client node.

Bridge mode options:
    -b, --bridge                        Run in bridge mode.
    -u LIST, --update=LIST              Run a smaller bootstrap updater bridge.  LIST can be blank in which case all bootstrap sources from the config file will be updated, or LIST can be a comma separated list of individual bootstrap sources to update.

Debug mode options:
    These should be available to nodes running from source code only. Typically, this will only be used by developers.
    
    -v, --verbose                       Set the logging output to provide more information.
    -l PORT, --listen=PORT              Listen for the DC client on localhost:PORT. If none is given, the previous setting will be used; or port 7314 if this is the first run.
    -e, --emulate=NAME                  Run a specific subset emulation suite.
    -r, --run-tests=NAME                Run a specific subset of the test suites to verify functionality.

