.. _dev-arch:

Architecture
============

Main function
-------------

boot
	Bootstrap engine - collects the bootstrap information for a network when Gehyra starts. Has a :ref:`module interface <api-boot>`.
bridge
	Bridge directory.  Contains the bridge specific code, particularly the IRC modules, also in a drag\&drop manner.
client
	Client protocol implementations, such as ADC/NMDC.
client/bot
	Client-level bots. Has a :ref:`module interface <api-dcbot>`.
config
	Configuration engine. Chooses the appropriate network, and finds and reads its configuration file(s). Also maintains state for that network.
core
	Implements the :ref:`Gehyra protocol <dev-protocol>`.
transport
	Transport layer. Translates from abstract Gehyra packets to real data to be sent across the network. Has a :ref:`module interface <api-transport>`.
common
	Common code.  Code such as interface definition or utility functions used all over the codebase.


Tests
-----

emul
	Emulation routines for testing a subsets of the codebase without needing to be connected to the network or to a test
test
	Regression test suites.




