.. Gehyra documentation master file, created by
   sphinx-quickstart on Wed Jul 14 00:31:59 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Welcome to Gehyra's documentation!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.. toctree::
    :maxdepth: 2

    guide/index.rst
    api/index.rst
    dev/index.rst
    musings/index.rst


See `Documenting Python <http://docs.python.org/documenting/index.html>`_ for
tutorials on Sphinx, and links to other Sphinx-related resources.

:ref:`genindex` - :ref:`modindex` - :ref:`search`

