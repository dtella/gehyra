#!/bin/bash

MY_PATH=$(readlink -f $(pwd)/$0)
RC_PATH=$(dirname $MY_PATH)/pylintrc
GH_PATH=$(dirname $(dirname $MY_PATH))

cd $GH_PATH

if [[ "$1" = "dev" || "$1" = "rel" ]]
then
    (pylint --rcfile $RC_PATH-$1 gehyra; echo) | \
	# Sed out "E0213 - should use self as first argument" wrt zope templates
	# Sed out "W0232 - class has no __init__" wrt zope templates
        # Sed out "W0231 - __init__ calls wrong super().__init__" for logger - it doenst...
        # Sed out "W0233 - __init__ should call super().__init__" for logger - it does...
	sed -e '/^E0213:[ 0-9]\+:I/d' \
	    -e '/^W0232:[ 0-9]\+:I/d' \
	    -e '/^W023[13]:[0-9 ]\+:Logger.__init__/d' \
	    | \
	# Delete the ****.. lines created for all files if they are consecuative.
        sed -e '/^\*\{12\}/ { N; /\n\*\{12\}/ D; /\n$/ d; };' -e '/^$/d;'

    pylint --rcfile $RC_PATH-$1 gehyra.py
else
    echo "Usage $0 {dev|rel}"
    exit 1
fi

