""" @package gehyra.entity.modules
Contains 'drag and drop' entity modules.
Package containing modules for entitys available to the client

All entities should implement the IEntity interface and expose themselves
as a class by the same name as the file

$Id$
"""

