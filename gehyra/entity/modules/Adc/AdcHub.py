# This file is part of Gehyra.
#
# Gehyra is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gehyra is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gehyra.  If not, see <http://www.gnu.org/licenses/>.

"""
@package gehyra.entity.modules.Adc.AdcHub
Provides an implementation of an Adc Hub which contains the
core logic.
$Id$
"""

"""
@file gehyra/entity/modules/Adc/AdcHub.py
Provides an implementation of an Adc Hub which contains the
core logic.
"""

from gehyra.entity.modules.Adc.AdcBaseProtocol import AdcBaseProtocol
# from gehyra.common.logger import LOG

import base64, tiger

class AdcHub(AdcBaseProtocol):
    """AdcHub extends AdcBaseProtocol to provide core hub logic"""
    
    def __init__(self, entity):
        """Constructor
        @param entity adc_hub entity which controls us
        """
        AdcBaseProtocol.__init__(self)

        ## refence to the adc_hub entity which controls us
        self.entity = entity
        ## blah
        self.once = False

        ## blah
        self.inf = None

        ## the factory which created us will fill this in.
        self.factory = None

    def handle_SUP(self, **parts):
        """negociate login"""
        self.sendLine("ISUP ADBASE ADTIGR ADUCMD ADUCM0")
        self.sendLine("ISID AAAA")
        self.sendLine(r"IINF NIGehyra@Cambridge CT32"
                      "DEGehyra\sdistributed\shub")
        #self.sendLine(r"IINF NIGehyra CT1 DEGehyra\sBot I4127.0.0.1 I6[::1]")




    def handle_INF(self, **parts):
        """send request for pw"""

        nodecfg = self.entity.main.config_manager.node

        if (self.once is False and
            nodecfg['security']['password'] is True):
            self.sendLine("IGPA " + base64.b32encode(
                    tiger.hash(nodecfg['security']['salt'])))

        self.inf = parts['params']
        
        self.once = True

    def handle_PAS(self, **parts):
        """finish the login"""

        nodecfg = self.entity.main.config_manager.node

        if parts['params'][0] == nodecfg['security']['hash']:
            self.sendLine("BINF AAAA " + ' '.join(self.inf))
        else:
            self.sendLine("ISTA 123 Wrong\spassword")
