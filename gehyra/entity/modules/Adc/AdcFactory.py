# This file is part of Gehyra.
#
# Gehyra is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gehyra is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gehyra.  If not, see <http://www.gnu.org/licenses/>.

"""
@package gehyra.entity.modules.Adc.AdcFactory
Provides Adc Server Factories.
$Id$
"""

"""
@file gehyra/entity/modules/Adc/AdcFactory.py
Provides Adc Server Factories.
"""

from twisted.internet.protocol import ServerFactory
from gehyra.entity.modules.Adc.AdcHub import AdcHub

class AdcServerFactory(ServerFactory):
    """Extends twisted.internet.protocol.ServerFactory to associate
    the protocol it builds with the entity controling the protocol
    """
    def __init__(self, entity):
        """Constructor.
        @param entity which is responsible for the AdcHub this factory will
        create
        """
        ## refence to the adc_hub entity which needs an AdcHub object
        self.entity = entity

    def buildProtocol(self, addr):
        """Overrides ServerFactory.buildProtocol.
        Calls AdcHub constructor with the entity parameter, so that the hub has
        a reference to the entity which created it.
        """
        p = AdcHub(self.entity)
        p.factory = self
        return p
