# This file is part of Gehyra.
#
# Gehyra is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gehyra is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gehyra.  If not, see <http://www.gnu.org/licenses/>.

"""
@package gehyra.config.postprocess
Provides postprocessing functions for configuration data.
$Id$
"""

"""
@file gehyra/config/postprocess.py
Provides postprocessing functions for configuration data.
"""

# import os.path

# from gehyra.common.util import get_user_path
# from gehyra.common.logger import LOG

def postprocess_node(data):
    """Postprocess node.cfg data.
    @todo implement
    @param data Object containing typified data
    """
    pass

def postprocess_network(data):
    """Postprocess network.cfg data.
    @todo implement
    @param data Object containing typified data
    """
    pass

def postprocess_bridge(data):
    """Postprocess bridge.cfg data.
    @todo implement
    @param data Object containing typified data
    """
    pass

