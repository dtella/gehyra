# This file is part of Gehyra.
#
# Gehyra is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gehyra is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gehyra.  If not, see <http://www.gnu.org/licenses/>.

"""
@package gehyra.config.manager
Object for managing all configuration data.
Manages the configuration files available to the node.
$Id$
"""

"""
@file gehyra/config/manager.py
Object for managing all configuration data.
Manages the configuration files available to the node.
"""

# import os.path

# from gehyra.common.util import get_user_path
# from gehyra.common.logger import LOG
from gehyra.common.interfaces import IConfigManager

from gehyra.config.typedconfig import TypedINIConfig
from gehyra.config.locations import (get_node_path, get_network_path, 
                                     get_bridge_path, get_node_paths)

# from gehyra.config.postprocess import (postprocess_node, postprocess_network,
#                                        postprocess_bridge)

from zope.interface import implements

class Manager(object):
    """Configuration Manager singleton class.
    Collects all configuration infomation for the node.
    """
    implements(IConfigManager)

    def __init__(self, _):
        """Constructor.
        Assignes resonable default values to certain settings
        """

        ## @brief IConfigObject object containing the node data
        self.node = TypedINIConfig()
        ## @brief IConfigObject object containing the network data
        self.network = TypedINIConfig()
        ## @brief IConfigObject object containing the node data, if enabled
        self.bridge = None

    def initialize(self):
        """Initialize.
        Nothing to do
        """
        return True

    def load_config(self, node_config=None, bridge=False):
        """Loads the configuration files.
        Loads the configuration files.  Optionally takes a string pointing to a
        non default file

        @param node_config Optional string overriding the default node config file
        @param bridge Optional boolean value indicating if the bridge
        configuration data should be loaded
        """
        # Load the node.cfg first
        if self.node.load_default("node.cfg") is not True:
            return False

        if node_config is not None and len(node_config) > 0:
            _, cachepath, logpath = get_node_paths(node_config)
            self.node['paths']['cache'] = cachepath
            self.node['paths']['log'] = logpath

        self.node.load_real(get_node_path(node_config))
        
        # Then try network.cfg
        if self.network.load_default("network.cfg") is not True:
            return False

        self.network.load_real(get_network_path(self.node['paths']['network']))

        if bridge is True:

            # Then try bridge.cfg if the commandline tells us
            self.bridge = TypedINIConfig()
            if self.network.load_default("bridge.cfg") is not True:
                return False

            self.bridge.load_real(get_bridge_path(self.node['paths']['bridge']))

        return True
