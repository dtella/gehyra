# This file is part of Gehyra.
#
# Gehyra is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gehyra is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gehyra.  If not, see <http://www.gnu.org/licenses/>.

"""
@package gehyra.config.schema.network
A schema for network config files.
$Id$
"""

"""
@file gehyra/config/schema/network.py
A schema for network config files.
"""

from gehyra.config.postprocess import *
## @cond
network = ConfigSection(
"These settings are initially defined by the network operator, and define "
"unique identity of your network. They may be edited by users, but changing "
"some settings (such as the encryption keys) will cause their node to be "
"rejected by the rest of the network. ",
{
'hub_name': ConfigKey(type=str, desc=
    "The hub name, as seen by DC clients. By convention, this is often of the "
    'form "Gehyra@____".'),

'hub_desc': ConfigKey(type=str, desc=
    'The description of the "hub". If this is omitted, it will default to '
    '"[version string] on ``hub_name``".'),

'pub_key': ConfigKey(type=str, desc=
    "The public key for the master network signing key. This allows all nodes "
    "to verify bridge communications without needing the bootstrap data."),

'sym_key': ConfigKey(type=str, desc=
    "The symmetric key used to encrypt node-node communications. This "
    "essentially defines the uniqueness of a Gehyra network, so every network "
    "should have its own unique key."),

'subnets': ConfigKey(type=list, post=mksubnettree, desc=
    "List of CIDR subnets; Gehyra will only talk to nodes from these subnets. "
    "Bridge nodes are exempt from this; but they must provide authentication. "
    "Make sure you get this right initially, because you can't make changes "
    "once your network file has been distributed."),

'client_proto': ConfigKey(type=str, default="ADC", post=validate_protocol, desc=
    "The protocol used to communicate with the client. Valid values are "
    "``NMDC`` or ``ADC``. This may be reflected in the URI that the client "
    "uses to connect to Gehyra - either a ``nmdc://`` or ``adc://`` address."),

'force_crypto': ConfigKey(type=bool, default=False, desc=
    "Forces all client-client transfers to be in encrypted mode. Only "
    "effective if ``client_proto`` supports this - ADC does; NMDC does not. "),

})

boot = ConfigSection(
"This section defines bootstrap data sources, how to pull from them, and "
"other related settings. Each data source has a ``NAME``, which can be chosen "
"at the operator’s discretion. ",
{
'__NAME__.type': ConfigKey(type=str, post=validate_boot_type, desc=
    "Type of data source. Valid values are: dns, gdata, [TODO]."),

'__NAME__.args': ConfigKey(type=dict, post=validate_boot_args, desc=
    "Arguments passed to the puller's initializer for this data source. See "
    "the documentation for the appropriate puller for details."),

'priority': ConfigKey(type=list, desc=
    "A list of all the data sources ``__NAME__``s. This defines the order for "
    "running the bootstrap pullers. "),

'minshare_cap': ConfigKey(type=str, default='100MiB', post=format_bytes, desc=
    "This enforces a maximum cap for the \"minshare\" value which appears in "
    "DNS. It should be set to some sane value to prevent the person managing "
    "DNS from setting the minshare to 99999TiB, and effectively disabling the "
    "network."),

})

locations = ConfigSection(
"Here we configure a mapping from a user's hostname or IP to their location. "
'Locations are displayed in the "Connection / Speed" column of the DC client '
'in NMDC mode, or in a prefix tag in the "Description" column in ADC mode. ',
{
'enabled': ConfigKey(type=bool, default=False, desc=
    "Whether to enable locations"),

'cidr': ConfigKey(type=dict, default={}, desc=
    "Define how IP ranges map to locations. If a remote peer's IP is not "
    "matched by this, its own location claim (see host_regex below) will be "
    "used instead. This should be a map of {location:[subnets]}, where the "
    "subnets are in CIDR notation. Longest-prefix-match will be used, so you "
    "can define overlapping ranges as long as they are not equal."),

'host_rdns': ConfigKey(type=list, default=[], desc=
    "DNS servers which will be used for doing IP->Hostname reverse lookups. "
    "These should be set to your school's local DNS servers, for efficiency."),

'host_regex': ConfigKey(type=list, default=[], post=validate_host_regex, desc=
    "Define how DNS names map to locations. This will only be used to send a "
    "location claim about yourself to other peers; they may choose to ignore "
    "it. This should be a list of (regexp, results) tuples used to match a "
    "hostname. Search proceeds down the list, and will stop at the first "
    "match. The first matching subgroup will be used as the key into its "
    "results table, and the value will be returned as the location."),

})
## @endcond
