#!/usr/bin/python

"""unittest testcases for the tiger hash module"""

import unittest, base64, tiger, struct

OLDB32ENCODE = base64.b32encode
def newb32encode(val):
    """base32 encode without padding"""
    return OLDB32ENCODE(val).rstrip('=')
base64.b32encode = newb32encode

OLDB32DECODE = base64.b32decode
def newb32decode(val):
    """base32 decode without padding"""
    _ , rem = divmod(len(val), 8)
    return OLDB32DECODE(val + '='*(8-rem))
base64.b32decode = newb32decode


class TestTiger(unittest.TestCase):
    """test vector taken from:
    http://en.wikipedia.org/wiki/Tiger_%28cryptography%29
    http://www.cs.technion.ac.il/~biham/Reports/Tiger/test-vectors-nessie-format.dat
    """

    def test_null_string(self):
        """Set 1, vector#  0:"""
        val = tiger.hash("")
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2x" % i for i in ival ) )
        res = "3293ac630c13f0245f92bbb1766e16167a4e58492dde73f3"
        self.assertEqual(res, hexstr)

    def test_a(self):
        """Set 1, vector#  1:"""
        val = tiger.hash("a")
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2X" % i for i in ival ) )
        res = "77BEFBEF2E7EF8AB2EC8F93BF587A7FC613E247F5F247809"
        self.assertEqual(res, hexstr)

    def test_abc(self):
        """Set 1, vector#  2:"""
        val = tiger.hash("abc")
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2X" % i for i in ival ) )
        res = "2AAB1484E8C158F2BFB8C5FF41B57A525129131C957B5F93"
        self.assertEqual(res, hexstr)

    def test_message_digest(self):
        """Set 1, vector#  3:"""
        val = tiger.hash("message digest")
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2X" % i for i in ival ) )
        res = "D981F8CB78201A950DCF3048751E441C517FCA1AA55A29F6"
        self.assertEqual(res, hexstr)

    def test_single_alphabet(self):
        """Set 1, vector#  4:"""
        val = tiger.hash("abcdefghijklmnopqrstuvwxyz")
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2X" % i for i in ival ) )
        res = "1714A472EEE57D30040412BFCC55032A0B11602FF37BEEE9"
        self.assertEqual(res, hexstr)

    def test_zigzag_alphabet(self):
        """Set 1, vector#  5:"""
        val = tiger.hash(
            "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq")
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2X" % i for i in ival ) )
        res = "0F7BF9A19B9C58F2B7610DF7E84F0AC3A71C631E7B53F78E"
        self.assertEqual(res, hexstr)

    def test_dotted_alphabet(self):
        """Set 1, vector#  6:"""
        # val = tiger.hash("A...Za...z0...9") you would think this is
        # what the test case means?  but no
        val = tiger.hash(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2X" % i for i in ival ) )
        res = "8DCEA680A17583EE502BA38A3C368651890FFBCCDC49A8CC"
        self.assertEqual(res, hexstr)

    def test_numbers(self):
        """Set 1, vector#  7:"""
        val = tiger.hash("1234567890"*8)
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2X" % i for i in ival ) )
        res = "1C14795529FD9F207A958F84C52F11E887FA0CABDFD91BFD"
        self.assertEqual(res, hexstr)

    def test_million_as(self):
        """Set 1, vector#  8:"""
        val = tiger.hash("a"*1000000)
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2X" % i for i in ival ) )
        res = "6DB0E2729CBEAD93D715C6A7D36302E9B3CEE0D2BC314B41"
        self.assertEqual(res, hexstr)

    def test_quick_brown_fox(self):
        """From wikipedia"""
        val = tiger.hash("The quick brown fox jumps over the lazy dog")
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2x" % i for i in ival ) )
        res = "6d12a41e72e644f017b6f0e2f7b44c6285f06dd5d2c5b075"
        self.assertEqual(res, hexstr)

    def test_lazy_cog(self):
        """Ffrom wikipedia"""
        val = tiger.hash("The quick brown fox jumps over the lazy cog")
        ival = struct.unpack("24B", val)
        hexstr = ''.join( ( "%.2x" % i for i in ival ) )
        res = "a8f04b0f7201a0d728101c9d26525b31764a3493fcd8458f"
        self.assertEqual(res, hexstr)

    def test_pid_cid(self):
        """From StrongDC"""

        pid = "UALUM2EL5VF2TEDYL7KPYRHZRQH2KNAIGTVD6YQ"
        rpid = base64.b32decode(pid)
        hpid = tiger.hash(rpid)
        epid = base64.b32encode(hpid)

        cid = "VDQJWI7J7QYZAYU6LVWIMUG5AWXNG7G2R7ZJHGA"

        self.assertEqual(epid, cid)
    


if __name__ == "__main__":
    unittest.main()
