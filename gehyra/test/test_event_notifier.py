#!/usr/bin/python

import unittest, sys, logging

if __name__ == "__main__":
    sys.path.append("../../")

from gehyra.common.interfaces import IEventListener as EL
from gehyra.common.event_notifier import EventNotifier as EN

class TestEventNotifier(unittest.TestCase):

    def setUp(self):
        self.en = EN.getInstance()
        self.en.log.setLevel(logging.ERROR)
        self.en.listeners = []
        self.en.last_event = None
        self.assertTrue(len(self.en.listeners) == 0)
        self.assertTrue(self.en.last_event is None)
    
    def test_hook_single(self):
        self.en.hook_listener(self)
        
    def test_invalid_last_event(self):
        self.assertTrue(self.en.last_event == None)
        
    def test_hook_double(self):
        self.en.hook_listener(self)
        l = len(self.en.listeners)
        self.en.hook_listener(self)
        self.assertTrue(l == len(self.en.listeners))
        
    def test_unhook_empty(self):
        self.en.unhook_listener(self)
        
    def test_hook_unhook(self):
        self.en.hook_listener(self)
        self.en.unhook_listener(self)
        self.assertTrue(len(self.en.listeners) == 0)
     
    def test_notify_nothing(self):
        self.en.notify_event(EN.GEHYRA_STARTUP)
        
    def test_notify_invalid_objects(self):
        self.en.hook_listener(self)
        self.en.notify_event(EN.GEHYRA_STARTUP)
        
    def test_valid_notifier(self):
        class V(EL):
            def on_event(self,event):
                self.e = event
        v = V()
        self.en.hook_listener(v)
        self.en.notify_event(EN.GEHYRA_ONLINE)
        self.assertTrue(v.e == EN.GEHYRA_ONLINE)
        
    def test_valid_last_event(self):
        self.en.notify_event(EN.GEHYRA_STARTUP)
        self.assertTrue(self.en.last_event == EN.GEHYRA_STARTUP)
        self.en.notify_event(EN.GEHYRA_ONLINE)
        self.assertTrue(self.en.last_event == EN.GEHYRA_ONLINE)
        self.en.notify_event(EN.GEHYRA_OFFLINE)
        self.assertTrue(self.en.last_event == EN.GEHYRA_OFFLINE)
        self.en.notify_event(EN.GEHYRA_SHUTDOWN)
        self.assertTrue(self.en.last_event == EN.GEHYRA_SHUTDOWN)
        
if __name__ == "__main__":
    unittest.main()
