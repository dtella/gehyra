""" @package gehyra.bootstrap.modules
Contains 'drag and drop' boostrap modules.
Package containing small modules for dealing with bootstrapping and locations

All bootstrap sub modules should be in the form 'pull_@<NAME>.py' or
'push_@<NAME>.py' for modules which read from and update respectivly.  Each
module should implement a class by the same name as the file.  The classes
should implemet the IBootstrapPuller and IBootstrapPusher interfaces
respectivly.  Only a bridge should use the 'push_*.py' modules.

$Id$
"""

