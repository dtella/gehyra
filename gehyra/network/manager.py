#
# This file is part of Gehyra.
#
# Gehyra is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gehyra is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gehyra.  If not, see <http://www.gnu.org/licenses/>.

"""
@package gehyra.network.manager
Package containing all network level code.
$Id$
"""

"""
@file gehyra/core/gehyra_node.py
Package containing all network level code.
"""

# from gehyra.common.logger import LOG
from gehyra.common.interfaces import (IGlobalObserver, IManager)

from zope.interface import implements


class Manager(object):
    """Network Manager"""
    implements(IManager, IGlobalObserver)

    def __init__(self, main):
        """Constructor.
        @param main Main gehyra object
        """
        ## Reference to main gehyra object
        self.main = main

    def initialize(self):
        """Initialize.
        Hooks self as an observer to the main gehyra object
        """
        self.main.hook_observer(self)
        return True

    def on_event(self, event):
        """Observe event.
        If we hear a startup even, and the configuration says we should be persistent,
        request that we synchronise with the network anyway.
        """
        if event == self.main.EVENT_STARTUP:
            if self.main.config_manager.node['online']['persistent'] == True:
                self.main.request_sync()

    def on_state(self, state):
        """Observe state change."""
